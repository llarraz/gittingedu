import sys,subprocess
import yaml




class CourseGit(object):
    'Manage Courses with git'
    def __init__(self,file_conf_yaml):
        try:
            f=open(file_conf_yaml)
            self.d_conf=yaml.load(f)
            f.close()
        except FileNotFoundError:
            print('aa FileNotFound')
            self.after_exception()
        except PermissionError:
            print('permision error')
            self.after_exception()
        except UnicodeDecodeError:
            print('Not text file')
            self.after_exception()
        except yaml.parser.ParserError as e:
            print('yaml parse error')
            print(e)
            self.after_exception()
        except Exception as e:
            print('exception: {}'.format(e))
            self.after_exception()
        
        self.ids = [d['id'] for d in self.d_conf['students']]

    def after_exception(self):
        print('exiting')
        sys.exit(1)

    def clone_repos(self):
        self.cmds_clone = []
        base_dir = self.d_conf['base_dir_students']
        git_remote_students_base = self.d_conf['git_remote_students_base']
        for d in self.d_conf['students']:
            cmd = 'git clone ' + git_remote_students_base.format(
                                git_user  = d['git_user'],
                                repo_name = d['repo_name']
                            ) + ' "' + base_dir + '/' + d['id'] + '"'
            status,output = subprocess.getstatusoutput(cmd)
            print('\n ### id:{} ### Clonning repo ###'.format(d['id']))
            print('Command to clone: {}'.format(cmd))
            if status !=0:
                print('ERROR cloning repo: \n *student: {}\m *repo:{}'.format(d['name'],d['repo_name']))
                print(output)
            else:
                print('repo cloned OK')
            self.cmds_clone.append(cmd)